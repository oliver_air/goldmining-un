<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $country_id
 *
 * @property Country $country
 * @property Mining[] $minings
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'country_id'], 'required'],
            [['country_id'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinings()
    {
        return $this->hasMany(Mining::className(), ['company_id' => 'id']);
    }




    public function fields()
    {
        return [
            'id',
            'name',
            'email',
            'country_id',
            'country'
             ];
    }


    public function extraFields()
    {
        return [
            'country'
        ];
    }
}
