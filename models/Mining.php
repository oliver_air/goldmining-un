<?php

namespace app\models;

use Yii;
use Carbon\Carbon;
use yii\db\Query;

/**
 * This is the model class for table "mining".
 *
 * @property int $id
 * @property int $company_id
 * @property int $datetime
 * @property int $mined
 *
 * @property Company $company
 */
class Mining extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mining';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'datetime'], 'required'],
            [['company_id', 'datetime', 'mined'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'datetime' => 'Datetime',
            'mined' => 'Mined',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public static function getLimits()
    {
        return [
          'min' => 100,
          'max' => 10000000
        ];
    }

    public static function getAttempts()
    {
        return [
            'min' => 1,
            'max' => 3
        ];
    }

    public static function DayMining()
    {
        $attempts = self::getAttempts();
        $limits = self::getLimits();

        $attempt = rand($attempts['min'],$attempts['max']);

        for ($i=1; $i <= $attempt; $i++)
        {
            $limit[] = rand($limits['min'],$limits['max']);
        }

        return $limit;
    }

    public static function MiningMonths()
    {
        $months['min'] = Mining::find()->min('datetime');
        $months['max'] = Mining::find()->max('datetime');

        if($months['min'] != $months['max']) {
            $monthStart = Carbon::createFromTimestamp($months['min']);
            $monthFinish = Carbon::createFromTimestamp($months['max']);

            do {
                $data[$monthStart->month] = $monthStart->format('F');
                $monthStart->addMonth();
            } while ($monthStart->month < $monthFinish->month);
            return $data;
        }
        else
            return [];
    }

    public static function Report($month = 6)
    {

        $date = Carbon::now();
        $date->month = $month;
        $start = $date->startOfMonth()->timestamp;
        $end = $date->endOfMonth()->timestamp;
        $query = new Query;
        $data = $query->select(' `company_id`, SUM(`mined`) as `mined`, `co`.`name`, `cu`.`plan`')
            ->from('`mining` as `mi`')
            ->leftJoin('`company` as `co` ON `co`.`id` = `mi`.`company_id`')
            ->leftJoin('`country` as `cu` ON `cu`.`id` = `co`.`country_id`')
            ->where('`datetime` <= '.$end.' AND `datetime` >= '.$start)
            ->groupBy('`company_id`')
            ->having('SUM(`mined`) > `cu`.`plan`')
            ->orderBy('`cu`.`plan` DESC')
            ->all();
        return $data;
    }

    public static function ReportCountry($month = 6)
    {

        $date = Carbon::now();
        $date->month = $month;
        $start = $date->startOfMonth()->timestamp;
        $end = $date->endOfMonth()->timestamp;
        $query = new Query;
        $data = $query->select(' `country_id`, SUM(`mined`) as `mined`, `cu`.`name`, `cu`.`plan`')
            ->from('`mining` as `mi`')
            ->leftJoin('`company` as `co` ON `co`.`id` = `mi`.`company_id`')
            ->leftJoin('`country` as `cu` ON `cu`.`id` = `co`.`country_id`')
            ->where('`datetime` <= '.$end.' AND `datetime` >= '.$start)
            ->groupBy('`country_id`')
            ->having('SUM(`mined`) > `cu`.`plan`')
            ->orderBy('`cu`.`plan` DESC')
            ->all();
        return $data;
    }
}
