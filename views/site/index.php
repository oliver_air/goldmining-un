<?php

use yii\helpers\Url;
?>

<div class="col-lg-10 col-lg-offset-1">
    <div class="row" ng-init="selectedTab = 1;">
        <ul class="nav nav-tabs">
            <li class="active" ng-class="{active: selectedTab == 1}"><a ng-click="selectedTab = 1;" href="#">Countries</a></li>
            <li ng-class="{active: selectedTab == 2}"><a ng-click="selectedTab = 2;" href="#">Company</a></li>
            <li ng-class="{active: selectedTab == 3}"><a ng-click="selectedTab = 3;" href="#">Reports</a></li>
        </ul>
    </div>
    <div class="row">
        <div id="country" class="tab-content"  ng-show="selectedTab == 1">
            <div class="country-default-index" data-ng-controller="CountryController">
                <div>
                    <h2>All Countries <button type="button" class="btn btn-success btn-sm" ng-click="add()">
                           <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ADD
                        </button> </h2>
                    <div data-ng-show="countries.length > 0">
                        <table class="table table-striped table-hover">
                            <thead>
                            <th>Name</th>
                            <th>Plan</th>
                            <th>Actions</th>
                            </thead>
                            <tbody>
                            <tr data-ng-repeat="country in countries">
                                <td>{{country.name}}</td>
                                <td>{{weight(country.plan)}}</td>
                                <td><button type="button" class="btn btn-primary btn-sm" ng-click="edit(country.id)">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> EDIT
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm" ng-click="delete(country.id)">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE
                                    </button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div data-ng-show="country.length == 0">
                        No results
                    </div>
                </div>
                <script type="text/ng-template" id="CountryModal.html">
                    <div class="modal-header">
                        <h3 class="modal-title" id="modal-title">{{title}}</h3>
                    </div>
                    <div class="modal-body" id="modal-body">
                        <form>
                            <div class="form-group" ng-repeat="(key, item) in country">
                                <label ng-if="item.type != 'hidden'" for="{{key}}" class="text-capitalize">{{key}} ({{item.description}})</label>
                                <input ng-model="identity[key]" class="form-control" type="{{item.type}}" name="{{key}}" value="{{item.value}}" />
                            </div>
                        </form>
                        <div class="alert alert-danger" ng-show="error == 1">
                            <p ng-repeat="(key, item) in errorMessage">
                                <strong>Error!</strong> {{item.message}}
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="save(identity)">Save</button>
                        <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
                    </div>
                </script>
            </div>
        </div>
        <div id="company" class="tab-content" ng-show="selectedTab == 2">
            <div class="company-default-index" data-ng-controller="CompanyController">
                <div>
                    <h2>All Companies <button type="button" class="btn btn-success btn-sm" ng-click="add()">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ADD
                        </button></h2>
                    <div data-ng-show="companies.length > 0">
                        <table class="table table-striped table-hover">
                            <thead>
                            <th>Name</th>
                            <th>Plan</th>
                            <th>Country</th>
                            <th>Actions</th>
                            </thead>
                            <tbody>
                            <tr data-ng-repeat="company in companies">
                                <td>{{company.name}}</td>
                                <td>{{company.email}}</td>
                                <td>{{company.country.name}}</td>
                                <td><button type="button" class="btn btn-primary btn-sm" ng-click="edit(company.id)">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> EDIT
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm" ng-click="delete(company.id)">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> DELETE
                                    </button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div data-ng-show="companies.length == 0">
                        No results
                    </div>
                </div>
                <script type="text/ng-template" id="CompanyModal.html">
                    <div class="modal-header">
                        <h3 class="modal-title" id="modal-title">{{title}}</h3>
                    </div>
                    <div class="modal-body" id="modal-body">
                        <form>
                            <div class="form-group" ng-repeat="(key, item) in company">
                                <label ng-if="item.type != 'hidden'" for="{{key}}" class="text-capitalize">{{key}} ({{item.description}})</label>
                                <input ng-model="identity[key]" class="form-control" type="{{item.type}}" name="{{key}}" value="{{item.value}}" />
                            </div>
                            <div class="form-group"">
                            <label for="country_id" class="text-capitalize">Country</label>
                            <select name="country_id" ng-model="identity.country_id" class="form-control">
                                  <option ng-selected="company.country_id == country.id" ng-repeat="country in countries"  value="{{country.id}}">{{country.name}}</option>
                              </select>
                            </div>
                        </form>
                        <div class="alert alert-danger" ng-show="error == 1">
                            <p ng-repeat="(key, item) in errorMessage">
                                <strong>Error!</strong> {{item.message}}
                            </p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="save(identity)">Save</button>
                        <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
                    </div>
                </script>
            </div>
        </div>
        <div id="reports" class="tab-content" ng-show="selectedTab == 3">
            <div class="company-default-index" data-ng-controller="ReportsController">
                <h2>Reports  <button type="button" class="btn btn-success btn-sm" ng-click="generate()">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> GENERATE DATA
                    </button></h2>
                <div class="col-lg-12" >
                    <div class="alert alert-info" ng-show="generating == 1">
                        <p><img src="<?= Url::to('img/loader.gif'); ?>" /> Generating data</p>
                    </div>
                    <div class="alert alert-danger" ng-show="generating == 2">
                        <p>Something going wrong!</p>
                    </div>
                    <div class="alert alert-success" ng-show="generating == 3">
                        <p>Data is ready</p>
                    </div>
                </div>
                <div class="col-lg-12" ng-show="monthsavaible = 1">
                    <div class="col-lg-8">
                    <select name="monthreport" ng-model="report_data.month" class="form-control">
                        <option ng-repeat="(month,name) in months"  value="{{month}}">{{name}}</option>
                    </select>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-primary" type="button" ng-click="report(report_data)">Report</button>
                    </div>
                </div>
                <div class="col-lg-12" ng-show="report_data.generated = 1">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Name of company</th>
                        <th>Mined (Plan)</th>
                        </thead>
                        <tbody>
                        <tr data-ng-repeat="mined in mining">
                            <td>{{mined.name}}</td>
                            <td>{{weight(mined.mined)}} ( {{weight(mined.plan)}} )</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-12" ng-show="report_data.generated = 1">
                    <table class="table table-striped table-hover">
                        <thead>
                        <th>Contry</th>
                        <th>Mined</th>
                        <th>Plan</th>
                        </thead>
                        <tbody>
                        <tr data-ng-repeat="co_mining in country_mining">
                            <td>{{co_mining.name}}</td>
                            <td>{{weight(co_mining.mined)}}</td>
                            <td>{{weight(co_mining.plan)}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
