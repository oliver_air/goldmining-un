'use strict';

var app = angular.module('app');

app.service('CountryService', function($http) {
    this.get = function() {
        return $http.get('/country');
    };
    this.post = function (data) {
        return $http.post('/country', data);
    };
    this.put = function (id, data) {
        return $http.put('/country/' + id, data);
    };
    this.delete = function (id) {
        return $http.delete('/country/' + id);
    };
});

app.service('CompanyService', function($http) {
    this.get = function() {
        return $http.get('/company?expand=country');
    };
    this.post = function (data) {
        return $http.post('/company', data);
    };
    this.put = function (id, data) {
        return $http.put('/company/' + id, data);
    };
    this.delete = function (id) {
        return $http.delete('/company/' + id);
    };
});

app.service('MiningService', function($http) {

    this.generate = function () {
        return $http.get('/generate');
    };

    this.months = function () {
        return $http.get('/mining/month');
    };

    this.report = function (id) {
        return $http.get('/mining/report?month=' + id);
    };

    this.reportcountry = function (id) {
        return $http.get('/mining/reportcountry?month=' + id);
    };
});

app.service('countriesStorage', function () {
    var countries = {};
    this.getIndex = function (id){
      return countries.map(function(e) {
          return e.id;
      }).indexOf(id);
    };
    this.set  = function(data) {
        countries = data;
    };
    this.get = function() {
        return countries;
    };
    this.add = function(item) {
        countries.push(item);
        return this.get();
    };
    this.delete = function(id) {
        var index = this.getIndex(id);
        countries.splice(index,1);
        return this.get();
    };
    this.getOne = function(id) {
        var index = this.getIndex(id);
        return countries[index];
    }
    this.edit = function (id, data) {
        var index = this.getIndex(id);
        countries[index] = data;
        return this.get();
    }
});

app.service('companiesStorage', function () {
    var companies = {};
    this.getIndex = function (id){
        return companies.map(function(e) {
            return e.id;
        }).indexOf(id);
    };
    this.set  = function(data) {
        companies = data;
    };
    this.get = function() {
        return companies;
    };
    this.add = function(item) {
        companies.push(item);
        return this.get();
    };
    this.delete = function(id) {
        var index = this.getIndex(id);
        companies.splice(index,1);
        return this.get();
    };
    this.getOne = function(id) {
        var index = this.getIndex(id);
        return companies[index];
    }
    this.edit = function (id, data) {
        var index = this.getIndex(id);
        companies[index] = data;
        return this.get();
    }
});