'use strict';

var controllers = angular.module('controllers', []);


controllers.controller('CountryController', ['$scope', '$uibModal', 'CountryService', 'countriesStorage',
    function ($scope, $uibModal, CountryService, countriesStorage) {
        $scope.countries = [];

        CountryService.get().then(function (data) {
            if (data.status == 200)
                countriesStorage.set(data.data);
                $scope.countries = countriesStorage.get();
        }, function (err) {
            console.log(err);
        });

        $scope.add = function(){
            $scope.modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'CountryModal.html',
                controller : 'CountryModalController',
                controllerAs: '$ctrl',
                size: 'sm',
                resolve: {
                    editId: function () {
                        return 0;
                    }
                }
            });
        }

        $scope.edit = function(id){
            $scope.modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'CountryModal.html',
                controller : 'CountryModalController',
                controllerAs: '$ctrl',
                size: 'sm',
                resolve: {
                    editId: function () {
                        return id;
                    },
                }
            });
        }

        $scope.delete = function (id) {
            CountryService.delete(id).then(function (data) {
                if (data.status == 204) {
                    $scope.countries = countriesStorage.delete(id);
                }
            }, function (err) {
                console.log(err);
            });
        }

    }
]).controller('CountryModalController', ['$scope', '$uibModalInstance', 'editId', 'CountryService',  'countriesStorage',
    function ($scope, $uibModalInstance, editId, CountryService, countriesStorage) {
        $scope.country = {
            'name' : {
                'value': '',
                'description': 'of country',
                'type': 'text'
            },
            'plan' : {
                'value': 0,
                'description': 'weight in grams',
                'type': 'number'
            }
        };
        $scope.identity = {};
        $scope.error = 0;
        $scope.errorMessage = {};

        if(editId != 0)
        {
            $scope.title = 'Edit country';
            var _data = countriesStorage.getOne(editId);
            $scope.country.name.value = _data.name;
            $scope.country.plan.value = _data.plan;
            $scope.identity.name = _data.name;
            $scope.identity.plan = _data.plan;
            console.log( $scope.country);
        }
        else
        {
            $scope.title = 'Add country';
        }

        $scope.cancel = function () {
            $uibModalInstance.close(false);
        };

        $scope.save = function (identity) {
            if (editId != 0)
            {
                CountryService.put(editId, identity, $scope).then(function (data) {
                    if (data.status == 200)
                        countriesStorage.edit(editId, data.data);
                        $scope.countries = countriesStorage.get();
                        $scope.cancel();
                }, function (err) {
                    $scope.error = 1;
                    $scope.errorMessage = err.data;
                    console.log(err);
                });
            }
            else
            {
                CountryService.post(identity).then(function (data) {
                    if (data.status == 201) {
                        countriesStorage.add(data.data);
                        $scope.countries = countriesStorage.get();
                        $scope.cancel();
                    }
                }, function (err) {
                    $scope.error = 1;
                    $scope.errorMessage = err.data;
                    console.log(err);
                });
            }
        };
    }
]);

controllers.controller('CompanyController', ['$scope', '$uibModal', 'CompanyService','companiesStorage',
    function ($scope, $uibModal, CompanyService, companiesStorage) {
        $scope.companies = [];
        $scope.identity = {
            'country_id' : '1'
        };
        CompanyService.get().then(function (data) {
            if (data.status == 200)
                companiesStorage.set(data.data);
                $scope.companies = companiesStorage.get();
        }, function (err) {
            console.log(err);
        });

        $scope.add = function(){
            $scope.modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'CompanyModal.html',
                controller : 'CompanyModalController',
                controllerAs: '$ctrl',
                size: 'sm',
                resolve: {
                    editId: function () {
                        return 0;
                    }
                }
            });
        };

        $scope.edit = function(id){
            $scope.modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'CompanyModal.html',
                controller : 'CompanyModalController',
                controllerAs: '$ctrl',
                size: 'sm',
                resolve: {
                    editId: function () {
                        return id;
                    },
                }
            });
        }

        $scope.delete = function (id) {
            CompanyService.delete(id).then(function (data) {
                if (data.status == 204) {
                    $scope.companies = companiesStorage.delete(id);
                }
            }, function (err) {
                console.log(err);
            });
        }
    }
]).controller('CompanyModalController', ['$scope', '$uibModalInstance', 'editId', 'CompanyService',  'companiesStorage', 'countriesStorage',
    function ($scope, $uibModalInstance, editId, CompanyService, companiesStorage, countriesStorage) {
        $scope.company = {
            'name' : {
                'value': '',
                'description': 'of company',
                'type': 'text'
            },
            'email' : {
                'value': '',
                'description': 'of company',
                'type': 'text'
            }
        };
        $scope.countries = countriesStorage.get();;
        $scope.identity = {
            'country_id' : '1'
        };
        $scope.error = 0;
        $scope.errorMessage = {};

        if(editId != 0)
        {
            $scope.title = 'Edit company';
            var _data = companiesStorage.getOne(editId);
            var country_id = ''+_data.country_id;
            $scope.company.name.value = _data.name;
            $scope.company.email.value = _data.email;
            $scope.identity.name = _data.name;
            $scope.identity.email = _data.email;
            $scope.identity.country_id = country_id;
            console.log($scope.identity);
        }
        else
        {
            $scope.title = 'Add company';
        }

        $scope.cancel = function () {
            $uibModalInstance.close(false);
        };

        $scope.save = function (identity) {
            if (editId != 0)
            {
                CompanyService.put(editId, identity, $scope).then(function (data) {
                    if (data.status == 200) {
                        companiesStorage.edit(editId, data.data);
                        $scope.companies = companiesStorage.get();
                        $scope.cancel();
                    }
                }, function (err) {
                    $scope.error = 1;
                    $scope.errorMessage = err.data;
                    console.log(err);
                });
            }
            else
            {
                CompanyService.post(identity).then(function (data) {
                    if (data.status == 201) {
                        companiesStorage.add(data.data);
                        $scope.countries = companiesStorage.get();
                        $scope.cancel();
                    }
                }, function (err) {
                    $scope.error = 1;
                    $scope.errorMessage = err.data;
                    console.log(err);
                });
            }
        };
    }
]);

controllers.controller('ReportsController', ['$scope', 'MiningService',
    function ($scope, MiningService) {
        $scope.mining = [];
        $scope.country_mining = [];
        $scope.generating = 0;
        $scope.monthsavaible = 0;
        $scope.report_data = {
           'month' : '6',
           'generated' : 0
        };
        MiningService.months().then(function (data){
            if(data.data != []) {
                $scope.monthsavaible = 1;
                $scope.months = data.data;
            }
            else {
                $scope.months = {};
            }

        });

        $scope.generate = function () {
            $scope.generating = 1;
            MiningService.generate().then(function (data) {
                if (data.status == 200)
                {
                    $scope.generating = 3;
                    MiningService.months().then(function (data){
                        if(data.data != []) {
                            $scope.months = data.data;
                            $scope.monthsavaible = 1;
                        }
                        else {
                            $scope.months = {};
                        }});
                }
            }, function (err) {
                $scope.generating = 2;
                console.log(err);
            })
        };
        
        $scope.report = function (data) {
            MiningService.report(data.month).then(function (data) {
                if (data.status == 200)
                {
                    $scope.report_data.generated = 1;
                    $scope.mining = data.data;
                }
            });

            MiningService.reportcountry(data.month).then(function (data) {
                if (data.status == 200)
                {
                    $scope.country_mining = data.data;
                }
            });
        }
    }
]);




