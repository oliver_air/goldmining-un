'use strict';

var app = angular.module('app', [,
    'ui.bootstrap',
    'controllers'
]);

app.run(function($rootScope) {
    $rootScope.weight = function(weight) {
        var response;
        if(weight < 1000)
        {
            response = weight+' g';
        }
        else if(weight >= 1000 && weight < 1000000)
        {
            response = weight/1000+' kg';
        }
        else if(weight >= 1000000)
        {
            response = weight/1000000+' T';
        }
        return response;
    };
});