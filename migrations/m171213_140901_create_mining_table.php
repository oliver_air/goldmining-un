<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mining`.
 */
class m171213_140901_create_mining_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('mining', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'datetime' => $this->integer()->notNull(),
            'mined' => $this->integer()->notNull()->defaultValue(100),
        ]);

        $this->createIndex(
            'idx-mining-company_id',
            'mining',
            'company_id'
        );

        $this->addForeignKey(
            'fk-mining-company_id',
            'mining',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('mining');
    }
}
