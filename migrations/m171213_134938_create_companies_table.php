<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies`.
 */
class m171213_134938_create_companies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'country_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-companies-country_id',
            'company',
            'country_id'
        );

        $this->addForeignKey(
            'fk-companies-country_id',
            'company',
            'country_id',
            'country',
            'id',
            'CASCADE'
        );

        $this->insert('company', [
            'name' => 'Goldcorp',
            'email' => 'goldcorp@mail.com',
            'country_id' => 1
        ]);

        $this->insert('company', [
            'name' => 'Barrick Gold',
            'email' => 'barrick@gold.com',
            'country_id' => 1
        ]);

        $this->insert('company', [
            'name' => 'Newmont Mining',
            'email' => 'newmont@mining.com',
            'country_id' => 2
        ]);

        $this->insert('company', [
            'name' => 'Polyus Gold',
            'email' => 'polyus@gold.com',
            'country_id' => 3
        ]);

        $this->insert('company', [
            'name' => 'Newcrest Mining',
            'email' => 'newcrest@mining.com',
            'country_id' => 4
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('company');
    }
}
