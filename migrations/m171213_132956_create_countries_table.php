<?php

use yii\db\Migration;

/**
 * Handles the creation of table `countries`.
 */
class m171213_132956_create_countries_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'plan' => $this->integer()->notNull()->defaultValue(0),
        ]);


        $this->insert('country', [
            'name' => 'Canada',
            'plan' => '10000000',
        ]);

        $this->insert('country', [
            'name' => 'USA',
            'plan' => '1000000',
        ]);

        $this->insert('country', [
            'name' => 'Russia',
            'plan' => '8000000',
        ]);

        $this->insert('country', [
            'name' => 'Australia',
            'plan' => '900000',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('country');
    }
}
