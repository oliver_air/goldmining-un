Goldmning.un for Boosters
===============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 7.0.
SETUP INSTRUCTIONS
-------------------

To run the project on your local server, you have to install Apache2 webserver,
PHP (PHP7 recommended) and MySQL server. Setup instructions can be easily found 
on the web.

Create Apache2 virtual hosts using these settings (*don't forget to
change the paths!*):
```
<VirtualHost *:80>
    ServerName goldmining.un

    ServerAdmin webmaster@localhost
    DocumentRoot "/var/www/goldmining.un/web"
	
    <Directory "/var/www/goldmining.un/web">
	AllowOverride All

        RewriteEngine on
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule . index.php

        DirectoryIndex index.php        
    </Directory>

</VirtualHost>
```
## Deployment guide

Install mysql `sudo apt install mysql mysql-client mysql-server`
Then install php7 and libraries `sudo apt install php7 php-xml php-mbstring php-memcached`
Install composer regarding to instruction `https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx`


Create database on your MySQL server 
``` 
    sudo mysql -u root 

    mysql> create database goldmining;
    mysql> exit
```

In project's directory execute:
```
    composer install
```
Then
```
    ./init
```
Choose suitable environment for you (development or production).
Then in created by this command file `common/config/main-local.php` set 
correct credentials and database name.

Then execute in project's directory:
```
    ./yii migrate
```
to apply migrations.

That's it!