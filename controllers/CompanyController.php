<?php

namespace app\controllers;

use function foo\func;
use yii\rest\ActiveController;

class CompanyController extends ActiveController
{
    public $modelClass = 'app\models\Company';

}
