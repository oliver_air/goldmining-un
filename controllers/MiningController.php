<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Mining;


class MiningController extends ActiveController
{
    public $modelClass = 'app\models\Mining';


    public function actionMonth()
    {
        return Mining::MiningMonths();
    }

    public function actionReport($month)
    {
        return Mining::Report($month);
    }

    public function actionReportcountry($month)
    {
        return Mining::ReportCountry($month);
    }
}
