<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use Carbon\Carbon;
use app\models\Company;
use app\models\Mining;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * Generating data for report.
     *
     * @return json
     */
    public function actionGenerate()
    {
        $now = Carbon::now();
        $forMonth = 6;

        if(($now->month - $forMonth) <= 0)
        {
            $month = abs($now->month - $forMonth);
            $year = $now->year - 1;
            $month = 12 - $month;
        }
        else
        {
            $year = $now->year;
            $month = $now->month - $forMonth;
        }
        $from = Carbon::createFromDate($year, $month, 1);

        $companies = Company::find()->all();

        Yii::$app->db->createCommand()->truncateTable('mining')->execute();


        do{
            foreach ($companies as $company)
            {
                $companyMiningDay = Mining::DayMining();
                foreach ($companyMiningDay as $miningDay)
                {
                    $mining = new Mining();

                    $mining->company_id = $company->id;
                    $mining->datetime = $from->timestamp;
                    $mining->mined = $miningDay;

                    if(!$mining->save())
                    {
                        $errors['error'] = 1;
                        $errors['message'][] = $mining->getErrors();
                    }
                }
            }
            $from->addDay();
        }
        while ($from < $now);

        if($errors['error'] != 1)
        {
            $errors['error'] = 0;
        }

        echo json_encode($errors);
    }

    public function actionMiningmonth()
    {
        echo json_encode(Mining::Report());
    }
}
